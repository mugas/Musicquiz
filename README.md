# Musicquiz

A music quiz made with JS

With just a piece of the lyrics, one has to discover the name of the band. In the end the score will be displayed. In every new game, a new set of colors will be displayed on each band name background.

Based on two tutorials. One from the [web Development Bootcamp](https://www.udemy.com/the-web-developer-bootcamp/), the [color game](https://github.com/mugas/colorfullgame).

After I had done the tutorial, I wanted to create my own version. It took me some time but felt good to see the end result and see how much I have improved after finishing it.


<ul>
<p><strong>Skills learned and/or reinforced</strong></p>
<li>For Loops</li>
<li>For each</li>
<li>Event handler</li>
<li>Object Oriented Programming</li>
<li>Functions</li>
<li>Keyword<em>this</em></li>
<li>Conditionals</li>
</ul>


